# Mozilla VPN GUI Client for Arch


## Getting started

1. [**Download** the latest release](https://gitlab.com/Linux-Is-Best/mozilla-vpn-gui-client-for-arch/-/releases)
1. Extract the archive
1. Open a terminal window in that folder (directory) 
1. Follow the directions below
  
## Pre-requirement

You can verify you have or install if needed by enterting the following command: `sudo pacman -S --needed base-devel git polkit libxcb libxmu libxrender libtiff libxdmcp dbus freetype2 qt6-charts qt6-declarative qt6-websockets qt6-imageformats qt6-networkauth qt6-svg qt6-5compat qt6-shadertools hicolor-icon-theme wireguard-tools cmake qt6-tools go flex clang cargo python-yaml python-lxml yamllint libcap ` The system will skip if you already have them or install them if needed. 

Addionally, you have the choice between **openresolv** and **systemd-resolvconf** (I personally, use systemd-resolvconf, but if you're not a fan of systemd, you can use openresolv).  

`sudo pacman -S --needed systemd-resolvconf ` **- OR -** `sudo pacman -S --needed openresolv `

Lastly, `yum -S python-glean_parser`

Option for Wayland support: `sudo pacman -S --needed qt6-wayland`

## Install / Upgrade

In your terminal window inside the directory (folder) you extracted the latest release, run the following command:


```
makepkg -si
```

## License

In simple English, this script which you use to install Mozilla VPN's GUI Client is free and open source and provided "as is". I welcome anyone to improve upon it and develop it further. But the actual client (program) and that this downloads and converts is owned by Mozilla and they retain all their respected rights.

## Project status

On going... If you notice Mozilla VPN has released a new package (https://github.com/mozilla-mobile/mozilla-vpn-client/releases) please **let me know** and I will update this.

I use [EndeavourOS](https://endeavouros.com/), by the way. - It's Arch Linux made easy. While I ensure the target (vanilla Arch Linux) will work, I am also confident [EndeavourOS](https://endeavouros.com/) will work too.

## Dear Mozilla VPN Team (if you're listening)

It would be **AWESOME** if you could officially support Arch Linux (and an RPM too). No one realistically expects you to support all the many offshoot distributions. You currently support Debian and Ubuntu, which are the base of many other distros. People who use another distro based on either of those (Debian or Ubuntu) know they are on their own. It would be cool if you did the same for Arch Linux (and perhaps an RPM too).  

In my opinion, someone such as myself should be a last resort.
